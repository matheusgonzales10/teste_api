**Desenvolvedor:** Matheus Moura Goncalves
**Contato:** (19) 97412-3274

## Instalacao
1. Faça um git clone
```
$ git clone https://gitlab.com/matheusgonzales10/teste_api.git
```
2. Vá para o diretório teste_api
```
$ cd teste_api
```
3. Copie `.env.example` para `.env` e modifique conforme suas variaveis de ambiente
```
$ cp .env.example .env
```
4. Modifique as variaveis de banco de dados
```
DB_CONNECTION=mysql
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
5. Instale as dependencias
```
$ composer install
```
6. Gere a chave da aplicação
```
$ php artisan key:generate
```
7. Rode as migrations para criar a tabela de Log
```
$ php artisan migrate
```
8. Gera swagger
```
php artisan l5-swagger:generate
```
9. Rode o servidor
```
php artisan serve
```

## Rotas

**Lista carros: **  api/listCarapi/listCars

------------
**Cria carro: **  api/createCar

------------
**Logs:**   api/logs

------------
**Documentação:**   api/documentacao