<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use DB;

use App\Models\Cars;
use App\Models\Logs;


class ApiController extends Controller
{
    
    /**
* @OA\Info(
*     title="API TESTE",
*     version="1.0.0"
*)
*/
    /**
     * @OA\Get(
     *     tags={"Carros"},
     *     summary="Retorna lista de carros",
     *     description="Retorna lista de carros",
     *     path="/api/listCars",
     *     @OA\Response(
     *      response=200,
     *      description="Retorna lista de carros",
     *      @OA\JsonContent(
     *          @OA\Property(property="_id", type="string", example="_id"),
     *          @OA\Property(property="title", type="string", example="title"),
     *          @OA\Property(property="brand", type="string", example="brand"),
     *          @OA\Property(property="price", type="string", example="price"),
     *          @OA\Property(property="age", type="string", example="age"),
     *      )
     *     ),
     * ),
     * 
    */
    
    public function listCars(request $request)
    {
        $cars = Cars::ListCars(); // Lista relação de carros
        
        return  Response::json($cars, 200);
    }

    /**
     * @OA\Post(
     *     tags={"Carros"},
     *     summary="Retorna carro criado",
     *     path="/api/createCar",
     *     @OA\Response(
     *      response=200,
     *      description="Retorna carro criado",
     *      @OA\JsonContent(
     *          @OA\Property(property="_id", type="string", example="_id"),
     *          @OA\Property(property="title", type="string", example="title"),
     *          @OA\Property(property="brand", type="string", example="brand"),
     *          @OA\Property(property="price", type="string", example="price"),
     *          @OA\Property(property="age", type="string", example="age"),
     *      )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       description="Exemplo de json",
     *       @OA\JsonContent(
     *           required={"title","brand","price","age"},
     *           @OA\Property(property="title", type="string", example="Fiat"),
     *           @OA\Property(property="brand", type="string",  example="Uno"),
     *           @OA\Property(property="price", type="string",  example="1000"),
     *           @OA\Property(property="age", type="string",  example="0"),
     *       ),
     *    )
     * ),
     *
    */
    
    public function createCar(request $request){
        
        
        $data = array(
            'title' => $request->title,
            'brand' => $request->brand,
            'price' => $request->price,
            'age'   => $request->age
        );

        $regras = Cars::regras(); //Regras de valição
        $mensagens = Cars::mensagens(); //Mensagens validação
        
        $validator = \Validator::make($data, $regras,$mensagens); //valida
        
        if ($validator->fails()) {  
            return Response::json(['errors'=>$validator->errors()], 400);
        }
        
        $car = Cars::createCar($data);

        $log = $this->salvaLog($car);
        
        return  Response::json(['car'=>$car,'log'=>$log], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Logs"},
     *     summary="Retorna logs de criacao de carros",
     *     description="Retorna logs de criacao de carros",
     *     path="/api/logs",
     *     @OA\Response(
     *      response=200,
     *      description="Retorna logs de criacao de carros",
     *      @OA\JsonContent(
     *          @OA\Property(property="_id", type="string", example="_id"),
     *          @OA\Property(property="title", type="string", example="title"),
     *          @OA\Property(property="brand", type="string", example="brand"),
     *          @OA\Property(property="price", type="string", example="price"),
     *          @OA\Property(property="age", type="string", example="age"),
     *      )
     *     ),
     * ),
     * 
    */
    
    public function salvaLog($data)
    {
        $log = new Logs;
        $log->data_hora    = date('Y-m-d H:i:s');
        $log->car_id       = $data->_id;
        $log->save();

        return $log;
    }

    public function listLogs(request $request)
    {
       $logs = Logs::get();

       return  Response::json(['logs'=>$logs], 200);
    }
}
