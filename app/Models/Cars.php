<?php

namespace App\Models;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\BadResponseException ;


use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    public static function regras()
    {
        $regras = array(
            'title' => 'required',
            'brand' => 'required',
            'price' => 'required',
            'age'   => 'required',
        );

        return $regras;
    }

    public static function mensagens()
    {
        $msg = array(
            'title.required' => 'O campo title é obrigátorio',
            'brand.required' => 'O campo brand é obrigátorio',
            'price.required' => 'O campo price é obrigátorio',
            'age.required'   => 'O campo age é obrigátorio',
        );

        return $msg;
    }

    public static function ListCars()
    {
        $url    = "http://api-test.bhut.com.br:3000/api/cars";
        $client = new Client();
       
        
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());

        return $response;
    }

    public static function createCar($data)
    {
        $data = json_encode($data);
        //dd($data);
        $client = new Client();
        $url    = "http://api-test.bhut.com.br:3000/api/cars";

        $response = $client->request('POST', $url,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $data
        ]);

        $response = json_decode($response->getBody());

        return $response;
    }
}
